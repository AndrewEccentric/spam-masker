package main

import (
	"context"
	"fmt"
	"os"
	"spam-masker/masklinks"
	"spam-masker/presenter"
	"spam-masker/producer"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Usage: go run main.go <input_file_path> <output_file_path>")
		return
	}

	ctx := context.Background()

	fileInput := os.Args[1]
	Producer := producer.New(fileInput)
	fileOutput := "output.txt"
	if len(os.Args) > 2 {
		fileOutput = os.Args[2]
	}
	Presenter := presenter.New(fileOutput)

	maskedMessagesChan := make(chan string, 100)

	maskingService := masklinks.NewService(Producer, Presenter, maskedMessagesChan, ctx)

	if err := maskingService.Run(ctx); err != nil {
		fmt.Printf("Error running the masking service: %v\n", err)
	}
}
