package producer

import (
	"bufio"
	"log/slog"
	"os"
)

type producer struct {
	fileName string
}

func New(fileName string) *producer {
	return &producer{
		fileName: fileName,
	}
}

func (s *producer) Produce() ([]string, error) {
	file, err := os.Open(s.fileName)
	if err != nil {
		slog.Error("Error opening file", slog.String("fileName", s.fileName), slog.String("error", err.Error()))
		return nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	var messages []string
	for scanner.Scan() {
		messages = append(messages, scanner.Text())
	}

	slog.Info("Produced messages", slog.Int("count", len(messages)))

	return messages, nil
}
