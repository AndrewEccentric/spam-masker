package presenter

import (
	"bufio"
	"log/slog"
	"os"
)

type presenter struct {
	fileName string
}

func New(fileName string) *presenter {
	return &presenter{
		fileName: fileName,
	}
}

func (s *presenter) Present(messages []string) error {
	file, err := os.Create(s.fileName)
	if err != nil {
		slog.Error("Error creating file", slog.String("fileName", s.fileName), slog.String("error", err.Error()))
		return err
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	for _, message := range messages {
		_, err := writer.WriteString(message + "\n")
		if err != nil {
			slog.Error("Error writing message", slog.String("message", message), slog.String("error", err.Error()))
			return err
		}
	}

	slog.Info("Presented messages", slog.Int("count", len(messages)))

	return writer.Flush()
}
