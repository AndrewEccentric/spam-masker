package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"spam-masker/masklinks"
	"spam-masker/presenter"
	"spam-masker/producer"
	"syscall"

	"log/slog"

	"github.com/urfave/cli/v2"
)

func parseLogLevel(levelStr string) (slog.Level, error) {
	switch levelStr {
	case "debug":
		return slog.LevelDebug, nil
	case "info":
		return slog.LevelInfo, nil
	case "warn":
		return slog.LevelWarn, nil
	case "error":
		return slog.LevelError, nil
	default:
		return slog.LevelInfo, fmt.Errorf("invalid log level: %v", levelStr)
	}
}

func runCLI() {
	app := &cli.App{
		Name:  "Spam Masker",
		Usage: "A tool to mask spam links in text files",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "input",
				Value: "input.txt",
				Usage: "Input file path",
			},
			&cli.StringFlag{
				Name:  "output",
				Value: "output.txt",
				Usage: "Output file path",
			},
			&cli.StringFlag{
				Name:  "log-level",
				Value: "info",
				Usage: "Log level (debug, info, warn, error)",
			},
		},
		Action: func(c *cli.Context) error {
			input := c.String("input")
			output := c.String("output")
			logLevel := c.String("log-level")

			level, err := parseLogLevel(logLevel)
			if err != nil {
				return err
			}

			logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
				Level: level,
			}))
			slog.SetDefault(logger)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			signalChan := make(chan os.Signal, 1)
			signal.Notify(signalChan, os.Interrupt, syscall.SIGTERM)

			go func() {
				sig := <-signalChan
				slog.Info("Received signal", slog.String("signal", sig.String()))
				cancel()
			}()

			Producer := producer.New(input)
			Presenter := presenter.New(output)

			maskedMessagesChan := make(chan string, 100)
			maskingService := masklinks.NewService(Producer, Presenter, maskedMessagesChan, ctx)

			if err := maskingService.Run(ctx); err != nil {
				slog.Error("Error running the masking service", slog.String("error", err.Error()))
				return err
			}

			slog.Info("Masking service completed successfully")
			return nil
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		slog.Error("Error running the application", slog.String("error", err.Error()))
	}
}
