package masklinks

func MaskLinks(message string) string {
	var maskedMessage []byte
	linkPrefix := []byte("http://")

	for i := 0; i < len(message); i++ {
		if len(message)-i >= len(linkPrefix) && message[i:i+len(linkPrefix)] == string(linkPrefix) {
			maskedMessage = append(maskedMessage, linkPrefix...)
			i += len(linkPrefix)
			for ; i < len(message) && message[i] != ' '; i++ {
				maskedMessage = append(maskedMessage, '*')
			}
			i--
		} else {
			maskedMessage = append(maskedMessage, message[i])
		}
	}

	return string(maskedMessage)
}
