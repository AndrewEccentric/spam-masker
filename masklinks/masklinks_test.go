package masklinks

import "testing"

func TestMaskLinks(t *testing.T) {
	tests := []struct {
		input, want string
	}{
		{
			input: "Here's my spammy page: http://hehefouls.netHAHAHA see you.",
			want:  "Here's my spammy page: http://******************* see you.",
		},
		{
			input: "http://hehefouls.netHAHAHA see you.",
			want:  "http://******************* see you.",
		},
		{
			input: "Here's my spammy page: http://hehefouls.netHAHAHA",
			want:  "Here's my spammy page: http://*******************",
		},
	}

	for _, test := range tests {
		test := test
		t.Run(test.input, func(t *testing.T) {
			got := MaskLinks(test.input)
			if got != test.want {
				t.Errorf("MaskLinks(%q) = %q, want %q", test.input, got, test.want)
			}
		})
	}
}
