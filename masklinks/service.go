package masklinks

import (
	"context"
	"log/slog"
	"sync"
)

type producer interface {
	Produce() ([]string, error)
}

type presenter interface {
	Present([]string) error
}

type Service struct {
	prod               producer
	pres               presenter
	maskedMessagesChan chan string
}

func NewService(prod producer, pres presenter, maskedMessagesChan chan string, ctx context.Context) *Service {
	return &Service{
		prod:               prod,
		pres:               pres,
		maskedMessagesChan: maskedMessagesChan,
	}
}

func (s *Service) Run(ctx context.Context) error {
	slog.Debug("Starting the service")

	messages, err := s.prod.Produce()
	if err != nil {
		return err
	}

	go func() {
		var wg sync.WaitGroup
		numGoroutines := 10
		sem := make(chan struct{}, numGoroutines)

		for _, message := range messages {
			select {
			case <-ctx.Done():
				slog.Warn("Context canceled, stopping goroutines")
				wg.Wait()
				close(s.maskedMessagesChan)
				return
			default:
				wg.Add(1)
				sem <- struct{}{}
				go func(msg string) {
					defer wg.Done()
					slog.Debug("Processing message", slog.String("message", msg))

					maskedMsg := MaskLinks(msg)
					select {
					case s.maskedMessagesChan <- maskedMsg:
					case <-ctx.Done():
						slog.Warn("Context canceled, stopping message processing")
						return
					}

					<-sem
				}(message)
			}
		}

		wg.Wait()
		close(s.maskedMessagesChan)
	}()

	var maskedMessages []string
	for maskedMessage := range s.maskedMessagesChan {
		maskedMessages = append(maskedMessages, maskedMessage)
	}

	if err := s.pres.Present(maskedMessages); err != nil {
		return err
	}

	return nil
}
