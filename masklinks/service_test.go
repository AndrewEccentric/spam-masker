package masklinks

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type MockProducer struct {
	mock.Mock
}

func (m *MockProducer) Produce() ([]string, error) {
	args := m.Called()
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).([]string), args.Error(1)
}

type MockPresenter struct {
	mock.Mock
}

func (m *MockPresenter) Present(messages []string) error {
	args := m.Called(messages)
	return args.Error(0)
}

type ServiceTestSuite struct {
	suite.Suite
	mockProducer       *MockProducer
	mockPresenter      *MockPresenter
	service            *Service
	maskedMessagesChan chan string
	ctx                context.Context
	cancel             context.CancelFunc
}

func (s *ServiceTestSuite) SetupTest() {
	s.mockProducer = new(MockProducer)
	s.mockPresenter = new(MockPresenter)
	s.maskedMessagesChan = make(chan string, 100)
	s.ctx, s.cancel = context.WithCancel(context.Background())
	s.service = NewService(s.mockProducer, s.mockPresenter, s.maskedMessagesChan, s.ctx)
}

func (s *ServiceTestSuite) TearDownTest() {
	s.cancel()
}

func (s *ServiceTestSuite) TestOkRun() {
	messages := []string{"http://something.com", "Hello, see you!"}
	maskedMessages := []string{MaskLinks("http://something.com"), "Hello, see you!"}
	s.mockProducer.On("Produce").Return(messages, nil)
	s.mockPresenter.On("Present", maskedMessages).Return(nil)

	err := s.service.Run(s.ctx)
	assert.NoError(s.T(), err)
	s.mockProducer.AssertExpectations(s.T())
	s.mockPresenter.AssertExpectations(s.T())
}

func (s *ServiceTestSuite) TestProduceError() {
	produceError := errors.New("produce error")
	s.mockProducer.On("Produce").Return(nil, produceError)

	err := s.service.Run(s.ctx)
	assert.Error(s.T(), err)
	assert.Equal(s.T(), produceError, err)
	s.mockProducer.AssertExpectations(s.T())
}

func (s *ServiceTestSuite) TestPresentError() {
	messages := []string{"http://something.com", "Hello, see you!"}
	maskedMessages := []string{MaskLinks("http://something.com"), "Hello, see you!"}
	presentError := errors.New("present error")
	s.mockProducer.On("Produce").Return(messages, nil)
	s.mockPresenter.On("Present", maskedMessages).Return(presentError)

	err := s.service.Run(s.ctx)
	assert.Error(s.T(), err)
	assert.Equal(s.T(), presentError, err)
	s.mockProducer.AssertExpectations(s.T())
	s.mockPresenter.AssertExpectations(s.T())
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, new(ServiceTestSuite))
}
